# MovexWeb

A simple front-end application for a moving company

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## How-to change endpoint
Currently the backend application has to be set manually within the various url callings within search-by-name-Component and place-order-Component
Currently set to 127.0.0.1:8080/order as prefix which is the rest endpoint to the movex spring boot application.
