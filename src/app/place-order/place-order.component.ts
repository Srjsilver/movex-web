import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
  orderForm: FormGroup;
  productForm: FormGroup;
  clear: boolean;
  orderNumber: Number;

  constructor(private _fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.reset();

  }

  validForm() {
    return this.orderForm.valid;
  }

  submit() {
    this.clear = false;
    const placeOrderUrl = 'http://127.0.0.1:8080/order/placeOrder';
    const form: FormGroup = this.orderForm;
    const jsonData: string = JSON.stringify(form.value);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    this.http.post(placeOrderUrl, jsonData, {headers, responseType: 'text'}).subscribe(
      data => {
        this.orderNumber = +data;
      },
      error => {
        console.log('an error has occured: ', error);
      }
    );
    // get ordernumber from response and assign to this.ordernumber
  }
  reset() {
    this.clear = true;
    this.orderNumber = 0;

    this.orderForm = this._fb.group({
      name: [
        '', Validators.required],
      phone: [
        '', Validators.required],
      email: [
        '', Validators.required],
      movingAddressFrom: [
        '', Validators.required],
      movingAdresssTo: [
        '', Validators.required],
      product: this._fb.group({
        isMoving: [
          true],
        isPacking: [
          true ],
        isCleaning: [
          true
        ]
      }),
      date: [
        '', Validators.required],
      time: [
        '', Validators.required],
      comments: [
        '']
    });
    this.productForm  = <FormGroup>(this.orderForm.get('product'));
  }
  show() {
    return this.clear;
  }
  getOrderNumber() {
    return this.orderNumber;
  }

}
