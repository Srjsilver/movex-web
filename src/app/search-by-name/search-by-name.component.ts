import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-search-by-name',
  templateUrl: './search-by-name.component.html',
  styleUrls: ['./search-by-name.component.css']
})
export class SearchByNameComponent implements OnInit {

  orderForm: FormGroup;
  productForm: FormGroup;
  searchForm: FormGroup;
  searchResults: boolean;
  pickedOneResult: boolean;
  searchResultData: JSON[];
  doShowSelected: boolean;
  orderUpdated: boolean;

  constructor(private _fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.orderUpdated = false;
    this.searchResults = false;
    this.pickedOneResult = false;
    this.doShowSelected = false;
    this.productForm  = this._fb.group({
      isMoving: [
        'false'],
      isPacking: [
        'false' ],
      isCleaning: [
        'false'
      ]
    });

    this.orderForm = this._fb.group({
      name: [
        '', Validators.required],
      phone: [
        '', Validators.required],
      email: [
        '', Validators.required],
      movingAddressFrom: [
        '', Validators.required],
      movingAdresssTo: [
        '', Validators.required],
      product: this._fb.group(this.productForm),
      date: [
        '', Validators.required],
      time: [
        '', Validators.required],
      comments: [
        '']
    });

    this.searchForm = this._fb.group({
      name: [
        '', Validators.required]
    });
  }
  backToSearch(){
    this.orderUpdated = false;
    this.searchResults = false;
    this.pickedOneResult = false;
    this.doShowSelected = false;
  }

  submit() {
    const placeOrderUrl = 'http://127.0.0.1:8080/order/placeOrder';
    const form: FormGroup = this.orderForm;
    const jsonData: string = JSON.stringify(form.value);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    this.http.post(placeOrderUrl, jsonData, {headers, responseType: 'text'}).subscribe(
      data => {
        this.searchResults = false;
        this.pickedOneResult = false;
        this.doShowSelected = false;
      },
      error => {
        console.log('an error has occured: ', error);
      }
    );
  }


  validForm() {
    return this.orderForm.valid;
  }

  doSelect(orderToBeDisplayed: any) {
    this.orderForm = this._fb.group(orderToBeDisplayed);
    this.doShowSelected = true;
  }

  show() {
    return this.doShowSelected;
  }

  doDelete(orderToBeDeleted: any) {
    const order: FormGroup = this._fb.group(orderToBeDeleted);
    const deleteOrderUrl = 'http://127.0.0.1:8080/order/deleteOrder/' + order.get('orderNumber').value;
    console.log(deleteOrderUrl);
    const number = <Number>order.get('orderNumber').value;
    this.http.delete(deleteOrderUrl).subscribe(
      data => {
        const isDeleted = <Boolean> data;
        if (data === true) {
         this.searchResultData = null;
          this.orderUpdated = false;
          this.searchResults = false;
          this.pickedOneResult = false;
          this.doShowSelected = false;
        }
      },
      error => {
        console.log('an error has occured: ', error);
      }
    );
  }
  hasSearchResults() {
    // no results to be shown
    return this.searchResults;
  }
  searchBarVisible() {
    // search bar visibility
    return this.hasSearchResults() === false && this.show() === false;
  }
  doSearch() {
    const searchNameLikeUrl = 'http://127.0.0.1:8080/order/findOrderByNameSearch/' + this.searchForm.get('name').value;
    this.http.get(searchNameLikeUrl).subscribe(
      data => {
        this.handleListOfJsonData(data);
      },
      error => {
        console.log('an error has occured: ', error);
      }
    );
    // http call to searchByName, with searchForm.name as input
    // if results then put empty
  }
  handleListOfJsonData(data: any) {
    this.searchForm.reset();
    this.searchResultData = data;
    if (this.searchResultData.length > 0) {
      this.searchResults = true;
    }
  }
  pickOrder(pickedOrder: JSON) {
    this.searchResults = false;
  }

  isOrderUpdated() {
    return this.orderUpdated;
  }
}
