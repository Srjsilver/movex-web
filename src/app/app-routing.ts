import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlaceOrderComponent} from './place-order/place-order.component';
import {SearchByNameComponent} from './search-by-name/search-by-name.component';

const routes: Routes = [
  {path: '', redirectTo: '/placeOrder', pathMatch: 'full'},
  {path: 'placeOrder', component: PlaceOrderComponent},
  {path: 'searchByName', component: SearchByNameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
